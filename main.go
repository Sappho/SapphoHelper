package main

import (
	"fmt"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/muesli/reflow/indent"
)

type Model struct {
	languages       []string
	languagesCursor int
	languageChosen  bool

	templates       map[string][]string
	templatesCursor int
	templateChosen  bool

	nameInput  textinput.Model
	nameChosen bool

	confirmed bool
}

func initialModel() Model {
	ti := textinput.New()
	ti.Placeholder = "Super Gay Project"
	ti.Focus()
	ti.CharLimit = 32
	ti.Width = 32
	m := Model{
		languages:      []string{"Java", "Kotlin", "Go", "Rust"},
		languageChosen: false,
		templates: map[string][]string{
			"Java":   {"Fabric Mod", "Architectury Mod"},
			"Kotlin": {"Fabric Mod", "Architectury Mod"},
			"Go":     {"BubbleTea TUI"},
			"Rust":   {"Valence Server", "eGui Application"},
		},
		templateChosen: false,
		nameInput:      ti,
		nameChosen:     false,
		confirmed:      false,
	}
	return m
}

func (m Model) Init() tea.Cmd {
	return nil
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	if msg, ok := msg.(tea.KeyMsg); ok {
		k := msg.String()
		if k == "esc" || k == "ctrl+c" || (k == "q" && m.nameChosen) || (k == "q" && !m.templateChosen) {
			return m, tea.Quit
		}
	}

	if !m.languageChosen {
		return updateLanguages(msg, m)
	}
	if m.languageChosen && !m.templateChosen {
		return updateTemplates(msg, m)
	}
	if m.languageChosen && m.templateChosen && !m.nameChosen {
		return updateName(msg, m)
	}
	if m.languageChosen && m.templateChosen && m.nameChosen && !m.confirmed {
		return updateConfirm(msg, m)
	}
	return m, tea.Quit
}

func (m Model) View() string {
	var s string
	if !m.languageChosen {
		s = languagesView(m)
	}
	if m.languageChosen && !m.templateChosen {
		s = templatesView(m)
	}
	if m.languageChosen && m.templateChosen && !m.nameChosen {
		s = nameView(m)
	}
	if m.languageChosen && m.templateChosen && m.nameChosen && !m.confirmed {
		s = confirmView(m)
	}
	if m.confirmed {
		s = finalView(m)
	}
	return indent.String("\n"+s+"\n\n", 2)
}

func updateLanguages(msg tea.Msg, m Model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "j", "down":
			if m.languagesCursor < len(m.languages)-1 {
				m.languagesCursor++
			}
		case "k", "up":
			if m.languagesCursor > 0 {
				m.languagesCursor--
			}
		case " ", "enter":
			m.languageChosen = true
		}
	}
	return m, nil
}

func updateTemplates(msg tea.Msg, m Model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "j", "down":
			if m.templatesCursor < len(m.templates[m.languages[m.languagesCursor]])-1 {
				m.templatesCursor++
			}
		case "k", "up":
			if m.templatesCursor > 0 {
				m.templatesCursor--
			}
		case " ", "enter":
			m.templateChosen = true
		}
	}
	return m, nil
}

func updateName(msg tea.Msg, m Model) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "enter":
			if len(m.nameInput.Value()) != 0 {
				m.nameChosen = true
			}
		}
	}
	m.nameInput, cmd = m.nameInput.Update(msg)
	return m, cmd
}

func updateConfirm(msg tea.Msg, m Model) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "y", "Y":
			m.confirmed = true
			return m, tea.Quit
		case "n", "N":
			m.languageChosen = false
			m.templatesCursor = 0
			m.templateChosen = false
			m.templatesCursor = 0
			m.nameChosen = false
			m.nameInput.SetValue("")
			return m, nil
		}
	}
	return m, nil
}

func separator() string {
	return lipgloss.NewStyle().Foreground(lipgloss.Color("242")).Render("-------------------------")
}

func header() string {
	s := fmt.Sprintf("%s\n%s\n",
		lipgloss.NewStyle().Foreground(lipgloss.Color("5")).Render("Sappho Helper"),
		separator())

	return s
}

func footer() string {
	s := fmt.Sprintf("%s\n%s\n",
		separator(),
		lipgloss.NewStyle().Foreground(lipgloss.Color("9")).Render("Press 'q' to quit."))
	return s
}

func languagesView(m Model) string {
	s := header()
	s += fmt.Sprintf("%s\n", activeSectionTitle("Language:"))
	for i, language := range m.languages {
		cursor := "-"
		if i == m.languagesCursor {
			cursor = "👉"
		}
		s += fmt.Sprintf("  %s %s\n", cursor, language)
	}
	s += footer()
	return s
}

func templatesView(m Model) string {
	s := header()
	s += fmt.Sprintf("%s %s\n", sectionTitle("Language:"), m.languages[m.languagesCursor])
	s += fmt.Sprintf("%s\n", activeSectionTitle("Template:"))
	for i, template := range m.templates[m.languages[m.languagesCursor]] {
		cursor := "-"
		if i == m.templatesCursor {
			cursor = "👉"
		}
		s += fmt.Sprintf("  %s %s\n", cursor, template)
	}
	s += footer()
	return s
}

func nameView(m Model) string {
	s := header()
	s += fmt.Sprintf("%s %s\n", sectionTitle("Language:"), m.languages[m.languagesCursor])
	s += fmt.Sprintf("%s %s\n", sectionTitle("Template:"), m.templates[m.languages[m.languagesCursor]][m.templatesCursor])
	s += fmt.Sprintf("%s %s\n", activeSectionTitle("Name:"), m.nameInput.View())
	s += footer()
	return s
}

func confirmView(m Model) string {
	s := header()
	s += fmt.Sprintf("%s %s\n", sectionTitle("Language:"), m.languages[m.languagesCursor])
	s += fmt.Sprintf("%s %s\n", sectionTitle("Template:"), m.templates[m.languages[m.languagesCursor]][m.templatesCursor])
	s += fmt.Sprintf("%s %s\n", sectionTitle("Name:"), m.nameInput.Value())
	s += fmt.Sprintf("%s %s\n", activeSectionTitle("Confirm:"), lipgloss.NewStyle().Foreground(lipgloss.Color("239")).Render("[y/n]"))
	s += footer()
	return s
}

func finalView(m Model) string {
	s := header()
	s += fmt.Sprintf("%s %s\n", sectionTitle("Language:"), m.languages[m.languagesCursor])
	s += fmt.Sprintf("%s %s\n", sectionTitle("Template:"), m.templates[m.languages[m.languagesCursor]][m.templatesCursor])
	s += fmt.Sprintf("%s %s\n", sectionTitle("Name:"), m.nameInput.Value())
	s += separator()
	return s
}

func sectionTitle(s string) string {
	return lipgloss.NewStyle().Foreground(lipgloss.Color("6")).Render(s)
}

func activeSectionTitle(s string) string {
	return lipgloss.NewStyle().Foreground(lipgloss.Color("2")).Render(s)
}

func main() {
	p := tea.NewProgram(initialModel())
	if _, err := p.Run(); err != nil {
		fmt.Println("Could not start program:", err)
	}
}
